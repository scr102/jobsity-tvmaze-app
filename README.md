# Jobsity TVMaze app

Application for listing TV series, using the API provided by the TVMaze
website.

The application was made using Jetpack Compose for the views, Hilt as 
dependency injection, Retrofit for API request and Room as Database
for the cache. The architecture used
was MVVM.

## Requirements

* Android Studio
* Git
* Android build tools v 30
* An Android phone 5.0 and up


## Common setup

Clone the repo and install the dependencies.

```bash
git clone https://gitlab.com/scr102/jobsity-tvmaze-app.git
```

Open the project on Android Studio and you are ready to go.

## APK

The "distribution" folder contain a signed APK of the app. 

