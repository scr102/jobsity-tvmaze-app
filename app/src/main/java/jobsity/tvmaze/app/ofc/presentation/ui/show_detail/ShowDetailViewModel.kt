package jobsity.tvmaze.app.ofc.presentation.ui.show_detail

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import jobsity.tvmaze.app.ofc.data.repositories.EspisodeRepositoryImpl
import jobsity.tvmaze.app.ofc.domain.model.episode.EpisodeItem
import kotlinx.coroutines.launch

const val STATE_KEY_SHOW_ID = "shows.state.showid.key"
const val STATE_KEY_LIST_POSITION = "shows.state.list_position.key"

class ShowsDetailViewModel @ViewModelInject constructor(
    private val episodeRepository : EspisodeRepositoryImpl,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    val episodelist : MutableState<List<EpisodeItem>> = mutableStateOf(listOf())

    val episodeSeasonlist : MutableState<List<EpisodeItem>> = mutableStateOf(listOf())

    val loading = mutableStateOf(false)

    val showId = mutableStateOf(1)

    val name = mutableStateOf("")

    val days = mutableStateOf("")

    val genres = mutableStateOf("")

    val poster = mutableStateOf("")

    val time = mutableStateOf("")

    val summary = mutableStateOf("")

    val season = mutableStateOf("1")

    val seasonList : MutableState<List<String>> = mutableStateOf(listOf())

    private var episodeListScrollPosition = 0

    init {
        savedStateHandle.get<Int>(STATE_KEY_LIST_POSITION)?.let { position ->
            setEpisodeListPosition(position)
        }
        if(episodeListScrollPosition != 0){
            onTriggerEvent(ShowDetailEvent.RestoreSateEvent)
        }

        loading.value = true;
    }

    fun onTriggerEvent(event : ShowDetailEvent){
        viewModelScope.launch {
            try{
                when(event){
                    is ShowDetailEvent.FetchEpisodeEvent -> {
                        fetchepisodes()
                    }
                    is ShowDetailEvent.RestoreSateEvent -> {
                        restoreState()
                    }
                }
            }catch(e: Exception){

            }

        }
    }

    private suspend fun fetchepisodes(){
        resetEpisodes()
        episodelist.value  = episodeRepository.getEpisodes(showId = showId.value)
        setSeasons()
        getEpisodesBySeason()
        loading.value = false
    }

    fun onSelectedSeasonChanged(s: String){
        season.value = s
    }

    fun getEpisodesBySeason(){
        resetEpisodes()
        var results = ArrayList<EpisodeItem>()
        for (e in episodelist.value){
            if(e.season == season.value.toInt()){
                results.add(e)
            }
        }
        episodeSeasonlist.value = results
    }

    private fun setSeasons(){
        var tempSeason = 1
        var result = ArrayList<String>()
        result.add("1")
        for (e in episodelist.value){
            if(e.season != tempSeason){
                result.add(e.season.toString())
                tempSeason = e.season!!
            }
        }
        seasonList.value = result
    }


    fun onChangeScrollPosition(position: Int){
        setEpisodeListPosition(position = position)
    }

    private fun setEpisodeListPosition(position: Int){
        episodeListScrollPosition = position
        savedStateHandle.set(STATE_KEY_LIST_POSITION,position)
    }

    private fun resetEpisodes(){
        episodeSeasonlist.value = listOf()
    }


    private suspend fun restoreState(){
        loading.value = true

        this.episodelist.value = episodeRepository.getEpisodes(showId = showId.value)

        loading.value = false
    }
}