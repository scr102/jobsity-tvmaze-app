package jobsity.tvmaze.app.ofc.domain.model.show

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Previousepisode(
    val href: String
) : Parcelable