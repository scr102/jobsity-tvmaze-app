package jobsity.tvmaze.app.ofc.presentation.components.anim

import androidx.compose.animation.core.*
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import jobsity.tvmaze.app.ofc.presentation.components.show_list.ShimmerShowCardItem

@Composable
fun LoadingShowListShimmer(
    coverHeight: Dp,
    coverWidth: Dp,
    textHeight: Dp,
    textWidth: Dp,
    padding: Dp = 16.dp,
){
    BoxWithConstraints (
        modifier = Modifier.fillMaxSize()
    ){
        val cardWidthPx = with(LocalDensity.current){(maxWidth - (padding*2)).toPx()}
        val cardHeightPx = with(LocalDensity.current){(maxHeight - (padding*2)).toPx()}
        val gradientWith : Float = (0.2f * cardHeightPx)

        val infiniteTransition = rememberInfiniteTransition()
        val xCardShimmer = infiniteTransition.animateFloat(
            initialValue = 0f,
            targetValue = (cardWidthPx + gradientWith),
            animationSpec = infiniteRepeatable(
                animation = tween(
                    durationMillis = 1300,
                    easing = LinearEasing,
                    delayMillis = 300,
                ),
                repeatMode = RepeatMode.Restart
            )
        )

        val yCardShimmer = infiniteTransition.animateFloat(
            initialValue = 0f,
            targetValue = (cardHeightPx + gradientWith),
            animationSpec = infiniteRepeatable(
                animation = tween(
                    durationMillis = 1300,
                    easing = LinearEasing,
                    delayMillis = 300,
                ),
                repeatMode = RepeatMode.Restart
            )
        )

        val colors = listOf(
            Color.LightGray.copy(alpha = .9f),
            Color.LightGray.copy(alpha = .3f),
            Color.LightGray.copy(alpha = .9f),
        )

        LazyColumn{
            items(5){
                ShimmerShowCardItem(
                    colors = colors,
                    xShimmer = xCardShimmer.value,
                    yShimmer = yCardShimmer.value,
                    coverHeight = coverHeight,
                    coverWidth = coverWidth,
                    textHeight = textHeight,
                    textWidth = textWidth,
                    gradientWidth = gradientWith,
                    padding = padding
                )
            }
        }
    }

}