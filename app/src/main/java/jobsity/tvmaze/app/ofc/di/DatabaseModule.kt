package jobsity.tvmaze.app.ofc.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import jobsity.tvmaze.app.ofc.data.local.EpisodesDao
import jobsity.tvmaze.app.ofc.data.local.ShowDatabase
import jobsity.tvmaze.app.ofc.data.local.ShowsDao
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
object DatabaseModule {

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): ShowDatabase {
        return Room.databaseBuilder(
            appContext,
            ShowDatabase::class.java,
            "show.db"
        ).build()
    }

    @Provides
    fun provideMovieDao(showDatabase: ShowDatabase): ShowsDao {
        return showDatabase.showsDao()
    }

    @Provides
    fun provideEpisodeDao(showDatabase: ShowDatabase): EpisodesDao {
        return showDatabase.episodesDao()
    }
}