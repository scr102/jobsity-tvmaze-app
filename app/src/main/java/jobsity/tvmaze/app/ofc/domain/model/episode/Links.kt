package jobsity.tvmaze.app.ofc.domain.model.episode

data class Links(
    val self: Self
)