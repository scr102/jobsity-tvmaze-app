package jobsity.tvmaze.app.ofc.presentation.ui.shows_list

import jobsity.tvmaze.app.ofc.domain.model.show.ShowItem

sealed class ShowsListEvent {

    object FetchShowsEvent : ShowsListEvent()

    object SearchShowsEvent : ShowsListEvent()

    object NextPageEvent : ShowsListEvent()

    object RestoreSateEvent: ShowsListEvent()
}