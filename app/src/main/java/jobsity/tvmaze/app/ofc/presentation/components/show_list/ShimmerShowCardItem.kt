package jobsity.tvmaze.app.ofc.presentation.components.show_list

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import jobsity.tvmaze.app.ofc.utils.Genre

@Composable
fun ShimmerShowCardItem(
    colors : List<Color>,
    xShimmer: Float,
    yShimmer: Float,
    coverHeight: Dp,
    coverWidth: Dp,
    textHeight: Dp,
    textWidth: Dp,
    gradientWidth: Float,
    padding: Dp,
){
    val brush = Brush.linearGradient(
        colors,
        start = Offset(xShimmer - gradientWidth ,yShimmer - gradientWidth),
        end= Offset(xShimmer,yShimmer)
    )
    Row(
        modifier = Modifier
            .padding(padding)
    ){
        Surface(shape= MaterialTheme.shapes.small) {
            Spacer(
                modifier = Modifier
                    .width(coverWidth)
                    .height(coverHeight)
                    .background(brush = brush)
            )
        }
        Spacer(modifier = Modifier.width(10.dp))
        Column (
            modifier = Modifier
                .fillMaxWidth()
        ){
            Surface(shape= MaterialTheme.shapes.small) {
                Spacer(
                    modifier = Modifier
                        .width(textWidth)
                        .height(textHeight)
                        .background(brush = brush)
                )
            }
            Spacer(modifier = Modifier.height(10.dp))

            Surface(shape= MaterialTheme.shapes.small) {
                Spacer(
                    modifier = Modifier
                        .width(textWidth*2)
                        .height(textHeight)
                        .background(brush = brush)
                )
            }
        }
    }


}