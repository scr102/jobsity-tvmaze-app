package jobsity.tvmaze.app.ofc.presentation.ui.shows_list

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import jobsity.tvmaze.app.ofc.domain.model.show.ShowItem
import jobsity.tvmaze.app.ofc.data.repositories.ShowRepository
import jobsity.tvmaze.app.ofc.data.repositories.ShowRepositoryImpl
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import jobsity.tvmaze.app.ofc.utils.Result

const val STATE_KEY_PAGE = "shows.state.page.key"
const val STATE_KEY_QUERY = "shows.state.query.key"
const val STATE_KEY_LIST_POSITION = "shows.state.list_position.key"

class ShowsListViewModel @ViewModelInject constructor(
    private val showRepository : ShowRepositoryImpl,
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    val showlist : MutableState<List<ShowItem>> = mutableStateOf(listOf())
    val query = mutableStateOf("")

    val loading = mutableStateOf(false)

    val page = mutableStateOf(1)

    val pageSize = mutableStateOf(0)

    val searching = mutableStateOf(false)

    private var showListScrollPosition = 0

    init {
        savedStateHandle.get<Int>(STATE_KEY_PAGE)?.let { p ->
            setPage(p)
        }
        savedStateHandle.get<Int>(STATE_KEY_LIST_POSITION)?.let { position ->
            setShowListPosition(position)
        }
        savedStateHandle.get<String>(STATE_KEY_QUERY)?.let { q ->
            setQuery(q)
        }
        if(showListScrollPosition != 0){
            onTriggerEvent(ShowsListEvent.RestoreSateEvent)
        }else{
            onTriggerEvent(ShowsListEvent.FetchShowsEvent)
        }
        loading.value = true;
    }

    fun onTriggerEvent(event : ShowsListEvent){
        viewModelScope.launch {
            try{
                when(event){
                    is ShowsListEvent.FetchShowsEvent -> {
                        fetchShows()
                    }
                    is ShowsListEvent.SearchShowsEvent -> {
                        searchShow()
                    }
                    is ShowsListEvent.NextPageEvent -> {
                        nextPage()
                    }
                    is ShowsListEvent.RestoreSateEvent -> {
                        restoreState()
                    }
                }
            }catch(e: Exception){

            }

        }
    }

    private suspend fun fetchShows(){
        resetSearchState()
        query.value = ""
        showlist.value  = showRepository.getShows(1)
        pageSize.value = showlist.value.size
        searching.value = false
        loading.value = false
    }

    private suspend fun searchShow(){
        resetSearchState()
        showlist.value  = showRepository.searchShows(query = query.value)
        searching.value = true
        loading.value = false

    }

    private suspend fun nextPage(){
        if((showListScrollPosition + 1) >= pageSize.value){
                loading.value = true
                incrementPage()
        }
        if(page.value > 1){
            val result = showRepository.getShows(page.value)
            appendShows(result)
        }
        loading.value = false
    }

    /**
     * Append shows to the current list
     */
    private fun appendShows(shows: List<ShowItem>){
        val current = ArrayList(this.showlist.value)
        current.addAll(shows)
        pageSize.value = current.size
        this.showlist.value = current
    }

    private fun incrementPage(){
        setPage(page.value + 1)

    }

    private fun setPage(page: Int){
        this.page.value = page
        savedStateHandle.set(STATE_KEY_PAGE,page)
    }

    fun onChangeScrollPosition(position: Int){
        setShowListPosition(position = position)
    }

    private fun setShowListPosition(position: Int){
        showListScrollPosition = position
        savedStateHandle.set(STATE_KEY_LIST_POSITION,position)
    }

    fun onQueryChanged(newQuery: String){
        setQuery(newQuery = newQuery)
        if(newQuery.isBlank()){
            onTriggerEvent(ShowsListEvent.FetchShowsEvent)
        }
    }

    private fun setQuery(newQuery: String){
        query.value= newQuery
        savedStateHandle.set(STATE_KEY_QUERY, newQuery)
    }

    fun onSearch(){
        resetSearchState()
        loading.value = true;
        onTriggerEvent(ShowsListEvent.SearchShowsEvent)
    }

    private fun resetSearchState(){
        showlist.value = listOf()
        pageSize.value = 0
        page.value = 1
        onChangeScrollPosition(0)
    }

    private suspend fun restoreState(){
        loading.value = true
        val results : MutableList<ShowItem> = mutableListOf()
        if(query.value.isBlank()) {
            for (p in 1..page.value) {
                val result = showRepository.getShows(page.value)
                results.addAll(result)
                if(p == page.value){
                    this.showlist.value = results
                }
            }
        }else{
            this.showlist.value = showRepository.searchShows(query.value)
        }
        loading.value = false
    }
}