package jobsity.tvmaze.app.ofc.utils

class Days {

    companion object{
        fun getDays(ids: List<String>?) : String? {
            ids?.let {
                val daysStrs = mutableListOf<String>()
                ids.forEach {
                    daysStrs.add(it)
                }
                return daysStrs.joinToString(separator = " | ")
            } ?: return  ""
        }
    }

}