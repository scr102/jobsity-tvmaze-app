package jobsity.tvmaze.app.ofc.data.remote

import jobsity.tvmaze.app.ofc.domain.model.episode.EpisodeItem
import jobsity.tvmaze.app.ofc.domain.model.show.ShowItem
import jobsity.tvmaze.app.ofc.network.ShowsService
import jobsity.tvmaze.app.ofc.network.model.episodes.EpisodeItemDtoMapper
import jobsity.tvmaze.app.ofc.network.model.show.SearchShowItemDtoMapper
import jobsity.tvmaze.app.ofc.network.model.show.ShowItemDtoMapper
import jobsity.tvmaze.app.ofc.utils.ErrorUtils
import jobsity.tvmaze.app.ofc.utils.Result
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class EpisodeRemoteDatasourceImpl @Inject constructor(
    private val retrofit: Retrofit,
    private val mapper: EpisodeItemDtoMapper,
) : EpisodeRemoteDatasource {

    override suspend fun getEpisodes(showId: Int): List<EpisodeItem> {
        val showsService = retrofit.create(ShowsService::class.java)
        return mapper.toDomainList(showsService.getEpisodes(showId = showId))
    }


}