package jobsity.tvmaze.app.ofc.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import jobsity.tvmaze.app.ofc.data.local.model.Shows

@Dao
interface ShowsDao {

    @Query("SELECT * FROM shows WHERE page = :page")
     suspend fun getShows(page: Int) : Shows?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
     suspend fun insertShows(show: Shows)

    @Query("DELETE FROM shows")
     suspend fun deleteAllShows()
}