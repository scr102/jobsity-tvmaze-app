package jobsity.tvmaze.app.ofc.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import jobsity.tvmaze.app.ofc.data.local.model.Episodes
import jobsity.tvmaze.app.ofc.data.local.model.Shows

@Dao
interface EpisodesDao {

    @Query("SELECT * FROM episodes WHERE showId = :showId")
     suspend fun getEpisodes(showId: Int) : Episodes?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
     suspend fun insertEpisode(episodes: Episodes)

    @Query("DELETE FROM episodes WHERE showId = :showId")
     suspend fun deleteEpisode(showId: Int)
}