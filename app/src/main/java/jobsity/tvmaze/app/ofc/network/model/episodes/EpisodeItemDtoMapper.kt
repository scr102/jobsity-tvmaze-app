package jobsity.tvmaze.app.ofc.network.model.episodes

import jobsity.tvmaze.app.ofc.domain.model.episode.EpisodeItem
import jobsity.tvmaze.app.ofc.domain.model.show.ShowItem
import jobsity.tvmaze.app.ofc.domain.util.DomainMapper


class EpisodeItemDtoMapper : DomainMapper<EpisodeItemDto, EpisodeItem>{
    override fun mapToDomainModel(model: EpisodeItemDto): EpisodeItem {
        return EpisodeItem(
                _links = model._links,
                airdate = model.airdate,
                airstamp = model.airstamp,
                airtime = model.airtime,
                id = model.id,
                image = model.image,
                name = model.name,
                number = model.number,
                runtime = model.runtime,
                season = model.season,
                summary = model.summary,
                type = model.type,
                url = model.url,

        )
    }

    override fun mapFromDomainModel(domainModel: EpisodeItem): EpisodeItemDto {
        return EpisodeItemDto(
            _links = domainModel._links,
            airdate = domainModel.airdate,
            airstamp = domainModel.airstamp,
            airtime = domainModel.airtime,
            id = domainModel.id,
            image = domainModel.image,
            name = domainModel.name,
            number = domainModel.number,
            runtime = domainModel.runtime,
            season = domainModel.season,
            summary = domainModel.summary,
            type = domainModel.type,
            url = domainModel.url
        )
    }

    fun toDomainList(initial: List<EpisodeItemDto>) : List<EpisodeItem>{
        return initial.map { mapToDomainModel(it) }
    }

    fun fromDomainList(initial: List<EpisodeItem>) : List <EpisodeItemDto> {
        return initial.map { mapFromDomainModel(it) }
    }
}