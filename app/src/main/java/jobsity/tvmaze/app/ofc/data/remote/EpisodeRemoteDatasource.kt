package jobsity.tvmaze.app.ofc.data.remote

import jobsity.tvmaze.app.ofc.domain.model.episode.EpisodeItem
import jobsity.tvmaze.app.ofc.domain.model.show.ShowItem

interface EpisodeRemoteDatasource {

    suspend fun getEpisodes(showId: Int): List<EpisodeItem>


}