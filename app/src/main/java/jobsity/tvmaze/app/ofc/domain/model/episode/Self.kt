package jobsity.tvmaze.app.ofc.domain.model.episode

data class Self(
    val href: String
)