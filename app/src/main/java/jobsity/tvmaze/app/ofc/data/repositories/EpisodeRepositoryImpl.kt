package jobsity.tvmaze.app.ofc.data.repositories

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import jobsity.tvmaze.app.ofc.data.local.ShowDatabase
import jobsity.tvmaze.app.ofc.data.local.model.Episodes
import jobsity.tvmaze.app.ofc.data.remote.EpisodeRemoteDatasourceImpl
import jobsity.tvmaze.app.ofc.domain.model.episode.EpisodeItem
import jobsity.tvmaze.app.ofc.domain.model.show.ShowItem

import javax.inject.Inject

class EspisodeRepositoryImpl @Inject constructor(
    private val remoteDatasource: EpisodeRemoteDatasourceImpl,
    private val localDatasource: ShowDatabase
) : EpisodeRepository {
    private val episodeDao = localDatasource.episodesDao()
    private val type = object : TypeToken<List<EpisodeItem>>() {}.type

    override suspend fun getEpisodes(showId: Int)  : List<EpisodeItem>{
        var result : List<EpisodeItem>
        try{
            result = remoteDatasource.getEpisodes(showId)
            episodeDao.deleteEpisode(showId)
            var episodes = Episodes(showId, Gson().toJson(result,type))
            episodeDao.insertEpisode(episodes)
        }catch (throwable : Throwable){
            result = getEpisodesCached(showId)
        }

        return result
    }


    private suspend fun getEpisodesCached(showId : Int) :List<EpisodeItem> =
        Gson().fromJson<List<EpisodeItem>>(episodeDao.getEpisodes(showId)?.episodesJson, type)

}