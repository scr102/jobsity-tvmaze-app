package jobsity.tvmaze.app.ofc.data.repositories

import jobsity.tvmaze.app.ofc.domain.model.episode.EpisodeItem

interface EpisodeRepository {
    suspend fun getEpisodes(page: Int):List<EpisodeItem>

}