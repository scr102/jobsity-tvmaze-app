package jobsity.tvmaze.app.ofc.domain.model.show

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Network(
    val country: Country,
    val id: Int,
    val name: String
) : Parcelable