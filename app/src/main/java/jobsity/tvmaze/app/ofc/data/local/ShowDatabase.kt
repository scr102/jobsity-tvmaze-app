package jobsity.tvmaze.app.ofc.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import jobsity.tvmaze.app.ofc.data.local.model.Episodes
import jobsity.tvmaze.app.ofc.data.local.model.Shows

@Database(entities = [Shows::class,Episodes::class], version = 1, exportSchema = false)
abstract class ShowDatabase : RoomDatabase(){

    abstract fun showsDao(): ShowsDao

    abstract fun episodesDao(): EpisodesDao
}