package jobsity.tvmaze.app.ofc.data.remote

import jobsity.tvmaze.app.ofc.domain.model.show.ShowItem
import jobsity.tvmaze.app.ofc.network.ShowsService
import jobsity.tvmaze.app.ofc.network.model.show.SearchShowItemDtoMapper
import jobsity.tvmaze.app.ofc.network.model.show.ShowItemDtoMapper
import jobsity.tvmaze.app.ofc.utils.ErrorUtils
import jobsity.tvmaze.app.ofc.utils.Result
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class ShowRemoteDatasourceImpl @Inject constructor(
    private val retrofit: Retrofit,
    private val mapper: ShowItemDtoMapper,
    private val searchMapper : SearchShowItemDtoMapper,
) : ShowRemoteDatasource {

    override suspend fun getShows(page: Int): List<ShowItem> {
        val showsService = retrofit.create(ShowsService::class.java)
        return mapper.toDomainList(showsService.getShows(page))
    }

    override suspend fun searchShows(query: String): List<ShowItem> {
        val showsService = retrofit.create(ShowsService::class.java)
        return searchMapper.toDomainList(showsService.searchShows(query))
    }

}