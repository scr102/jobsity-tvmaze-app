package jobsity.tvmaze.app.ofc.presentation.theme

import android.annotation.SuppressLint
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

@SuppressLint("ConflictingOnColor")
private val LightThemeColor = lightColors(
    primary = Green600,
    primaryVariant = Green400,
    secondary = Color.White,
    secondaryVariant = Teal300,
    background = Grey1,
    surface = Color.White,
    error = RedErrorDark,
    onPrimary = Black2,
    onSecondary = Color.Gray,
    onBackground = Color.Black,
    onSurface = Black2,
    onError = RedErrorLight
)

private val DarkThemeColor = darkColors(
    primary = Green700,
    primaryVariant = Color.White,
    secondary = Black1,
    secondaryVariant = Teal300,
    background = Color.Black,
    surface = Black1,
    error = RedErrorLight,
    onPrimary = Color.White,
    onSecondary = Color.White,
    onBackground = Color.White,
    onSurface = Color.White
)

@Composable
fun AppTheme(
    darkTheme: Boolean,
    content: @Composable () -> Unit,
){
    MaterialTheme(
        colors = if(darkTheme) DarkThemeColor else LightThemeColor
    ){
        content()
    }
}