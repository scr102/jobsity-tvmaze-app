package jobsity.tvmaze.app.ofc.presentation.ui.shows_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.material.Scaffold
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import dagger.hilt.android.AndroidEntryPoint
import jobsity.tvmaze.app.ofc.presentation.components.show_list.ShowList
import jobsity.tvmaze.app.ofc.presentation.components.show_list.SearchAppBar
import jobsity.tvmaze.app.ofc.presentation.theme.AppTheme

@AndroidEntryPoint
class ShowsListFragment : Fragment() {


    private lateinit var showsListViewModel: ShowsListViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        showsListViewModel =
            ViewModelProvider(this).get(ShowsListViewModel::class.java)


        return composeView()
    }


    private fun composeView(): ComposeView {
        return ComposeView(requireContext()).apply {
            setContent {

                AppTheme(
                    darkTheme = false
                ) {

                    val shows = showsListViewModel.showlist.value

                    val query = showsListViewModel.query.value

                    val loading = showsListViewModel.loading.value

                    val searching = showsListViewModel.searching.value

                    val pageSize = showsListViewModel.pageSize.value

                    Scaffold(
                        topBar = {SearchAppBar(
                            query = query,
                            onQueryChanged = showsListViewModel::onQueryChanged,
                            onSearch= showsListViewModel::onSearch,
                            onTriggerEvent = showsListViewModel::onTriggerEvent
                        )},
                    ) {
                        ShowList(
                            loading = loading,
                            shows = shows,
                            onChangeScrollPosition = showsListViewModel::onChangeScrollPosition,
                            onTriggerEvent = showsListViewModel::onTriggerEvent,
                            pageSize = pageSize,
                            searching = searching,
                            navController = findNavController()
                        )

                    }

                }

            }

        }

    }
}
