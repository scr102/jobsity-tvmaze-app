package jobsity.tvmaze.app.ofc.presentation.components.show_list

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import jobsity.tvmaze.app.ofc.R
import jobsity.tvmaze.app.ofc.presentation.ui.shows_list.ShowsListEvent
import jobsity.tvmaze.app.ofc.presentation.ui.shows_list.ShowsListViewModel

@Composable
fun SearchAppBar(
    query: String,
    onQueryChanged: (String) -> Unit ,
    onSearch: ()-> Unit,
    onTriggerEvent: (ShowsListEvent) -> Unit,
) {
    Surface(
        modifier = Modifier
            .fillMaxWidth()
            .background(Color(R.color.white)),
        color= Color.White,
        elevation = 10.dp
    ){

        Row(
            modifier = Modifier.fillMaxWidth()
        ){
            val keyboardController = LocalFocusManager.current
            TextField(
                modifier = Modifier
                    .fillMaxWidth(0.9f)
                    .padding(8.dp),
                value = query,
                onValueChange = {
                    onQueryChanged(it)
                },
                label = { Text(text = "Search") },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Text,
                    imeAction = ImeAction.Search
                ),
                leadingIcon = {
                    Icon(Icons.Filled.Search,"search")
                },
                trailingIcon = {

                    IconButton(onClick = {
                        onTriggerEvent(ShowsListEvent.FetchShowsEvent)
                    }) {
                        Icon(Icons.Filled.Close, "Delete")
                    }
                },
                keyboardActions = KeyboardActions(
                    onSearch = {
                        onSearch()
                        keyboardController.clearFocus()
                    }
                ),
                textStyle = TextStyle(
                    color = MaterialTheme.colors.onSurface),
                colors = TextFieldDefaults.textFieldColors(backgroundColor = MaterialTheme.colors.surface)
//

            )

        }
    }
}