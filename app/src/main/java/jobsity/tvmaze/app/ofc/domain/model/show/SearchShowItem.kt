package jobsity.tvmaze.app.ofc.domain.model.show

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SearchShowItem(
    val score: Float?,
    val show: ShowItem?,
        ) : Parcelable