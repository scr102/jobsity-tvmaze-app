package jobsity.tvmaze.app.ofc.presentation.theme

import androidx.compose.ui.graphics.Color

val Green300 = Color(0xFF62b4ad)
val Green400 = Color(0xFF4aa49b)
val Green500 = Color(0xFF3e948a)
val Green600 = Color(0xFF39877d)
val Green700 = Color(0xFF34776d)
val Green800 = Color(0xFF30675e)

val Teal300 = Color(0xFFe57579)

val Grey1 = Color(0xFFF2F2F2)

val Black1 = Color(0xFF222222)
val Black2 = Color(0xFF000000)

val RedErrorDark = Color(0xFFB00020)
val RedErrorLight = Color(0xFFEF5350)