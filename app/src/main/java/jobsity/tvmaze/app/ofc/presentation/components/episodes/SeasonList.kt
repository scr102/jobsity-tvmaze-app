package jobsity.tvmaze.app.ofc.presentation.components.episodes

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp


@Composable
fun SeasonList(
    seasons : List<String>,
    selectedSeason : String,
    onSelectedSeasonChanged : (String) -> Unit,
    onGetSeason : () -> Unit
) {
    val scrollState = rememberLazyListState()

    LazyRow(
        modifier = Modifier
            .padding(start = 8.dp, bottom = 8.dp),
        state = scrollState,
    ) {
        items(seasons) { season ->
            SeasonTab(
                season = season,
                isSelected = selectedSeason.equals(season),
                onSelectedSeasonChanged = {
                    onSelectedSeasonChanged(it)
                },
                onGetSeason
            )
        }
    }
}

