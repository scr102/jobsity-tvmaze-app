package jobsity.tvmaze.app.ofc.data.repositories

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import jobsity.tvmaze.app.ofc.data.local.ShowDatabase
import jobsity.tvmaze.app.ofc.data.local.model.Shows
import jobsity.tvmaze.app.ofc.data.remote.ShowRemoteDatasource
import jobsity.tvmaze.app.ofc.data.remote.ShowRemoteDatasourceImpl
import jobsity.tvmaze.app.ofc.domain.model.show.ShowItem
import jobsity.tvmaze.app.ofc.utils.Result
import kotlinx.coroutines.Dispatchers

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class ShowRepositoryImpl @Inject constructor(
    private val remoteDatasource: ShowRemoteDatasourceImpl,
    private val localDatasource: ShowDatabase
) : ShowRepository {
    private val showsDao = localDatasource.showsDao()
    private val type = object : TypeToken<List<ShowItem>>() {}.type

    override suspend fun getShows(page: Int)  : List<ShowItem>{
        var result : List<ShowItem>
        try{
            result = remoteDatasource.getShows(page)
            showsDao.deleteAllShows()
            var show = Shows(page, Gson().toJson(result,type))
            showsDao.insertShows(show)
        }catch (throwable : Throwable){
            result = getShowsCached(page)
        }

        return result
    }

    override suspend fun searchShows(query: String): List<ShowItem> {
        var result : List<ShowItem>

        try {
            result = remoteDatasource.searchShows(query)
        }catch (throwable : Throwable){
            result = listOf()
        }

        return result
    }

    private suspend fun getShowsCached(page : Int) :List<ShowItem> =
        Gson().fromJson<List<ShowItem>>(showsDao.getShows(page)?.showsJson, type)

}