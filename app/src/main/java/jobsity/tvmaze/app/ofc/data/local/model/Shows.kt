package jobsity.tvmaze.app.ofc.data.local.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Shows(
    @NonNull
    @PrimaryKey
    val page: Int,
    val showsJson: String?,
)

