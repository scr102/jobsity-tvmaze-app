package jobsity.tvmaze.app.ofc.presentation.ui.show_detail

sealed class ShowDetailEvent {

    object FetchEpisodeEvent : ShowDetailEvent()

    object RestoreSateEvent: ShowDetailEvent()
}