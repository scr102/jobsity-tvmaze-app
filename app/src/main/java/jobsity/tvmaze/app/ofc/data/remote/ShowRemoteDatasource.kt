package jobsity.tvmaze.app.ofc.data.remote

import jobsity.tvmaze.app.ofc.domain.model.show.ShowItem

interface ShowRemoteDatasource {

    suspend fun getShows(page: Int): List<ShowItem>

    suspend fun searchShows(query: String): List<ShowItem>


}