package jobsity.tvmaze.app.ofc.data.local.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Episodes (
    @NonNull
    @PrimaryKey
    val showId : Int?,
    val episodesJson: String?,
)