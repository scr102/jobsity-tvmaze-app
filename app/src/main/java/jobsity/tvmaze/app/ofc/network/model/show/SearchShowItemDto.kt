package jobsity.tvmaze.app.ofc.network.model.show

import jobsity.tvmaze.app.ofc.domain.model.show.ShowItem

data class SearchShowItemDto (
    val score : Float? = null,
    val show : ShowItem? = null,
        )