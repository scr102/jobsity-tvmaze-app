package jobsity.tvmaze.app.ofc.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import jobsity.tvmaze.app.ofc.data.repositories.ShowRepository
import jobsity.tvmaze.app.ofc.network.model.episodes.EpisodeItemDtoMapper
import jobsity.tvmaze.app.ofc.network.model.show.SearchShowItemDtoMapper
import jobsity.tvmaze.app.ofc.network.model.show.ShowItemDtoMapper
import jobsity.tvmaze.app.ofc.utils.Constants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(ApplicationComponent::class)
object NetworkModule {

    private const val baseUrl = Constants.BASE_URL

    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @Provides
    fun provideShowItemDtoMapper() : ShowItemDtoMapper{
        return ShowItemDtoMapper()
    }

    @Provides
    fun provideSearchShowItemDtoMapper() : SearchShowItemDtoMapper{
        return SearchShowItemDtoMapper()
    }

    @Provides
    fun provideEpisodeItemDtoMapper() : EpisodeItemDtoMapper{
        return EpisodeItemDtoMapper()
    }

    @Provides
    fun provideOkHttpClient() : OkHttpClient {
        return OkHttpClient()
    }

}