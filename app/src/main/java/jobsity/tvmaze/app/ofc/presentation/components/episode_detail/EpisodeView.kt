package jobsity.tvmaze.app.ofc.presentation.components.episode_detail

import android.graphics.Bitmap
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import jobsity.tvmaze.app.ofc.R
import jobsity.tvmaze.app.ofc.presentation.components.HtmlText
import jobsity.tvmaze.app.ofc.utils.IMAGE_PLACEHOLDER
import jobsity.tvmaze.app.ofc.utils.loadImage

@Composable
fun EpisodeView(
    name: String,
    number: String,
    season: String,
    image: String,
    summary: String,
) {
    val scrollState = rememberScrollState()

    Column(
        modifier = Modifier
            .verticalScroll(scrollState)
    ) {
        var imageBm: Bitmap? = null
        image?.let { url ->
            imageBm = loadImage(url = url, defaultImage = IMAGE_PLACEHOLDER).value
        } ?: run {
            imageBm = loadImage(url = "", defaultImage = IMAGE_PLACEHOLDER).value
        }

        imageBm?.let { img ->
            Image(
                bitmap = img.asImageBitmap(),
                stringResource(id = R.string.poster_image),
                modifier = Modifier
                    .fillMaxWidth()
                    .height(200.dp),
                contentScale = ContentScale.Crop
            )
        }
        Spacer(
            modifier = Modifier
                .height(20.dp)
        )

        name?.let { title ->
            Text(
                text = title,
                modifier = Modifier
                    .fillMaxWidth(0.85f)
                    .padding(start= 15.dp)
                    .wrapContentWidth(Alignment.Start),
                fontSize = 20.sp,
                color = MaterialTheme.colors.primary
            )
        }
        season?.let { season ->

            Text(
                text = "Season ${season}: E${number}",
                modifier = Modifier
                    .fillMaxWidth(0.85f)
                    .padding(start= 15.dp)
                    .wrapContentWidth(Alignment.Start),
                fontSize = 18.sp,
                color = MaterialTheme.colors.onSecondary
            )
        }

        Spacer(
            modifier = Modifier
                .height(20.dp)
        )

        Text(
            text = "Summary",
            modifier = Modifier
                .fillMaxWidth()
                .padding(start= 15.dp)
                .wrapContentWidth(Alignment.Start),
            fontWeight = FontWeight.Bold,
            fontSize = 24.sp,
            color = MaterialTheme.colors.onSecondary
        )
        Spacer(
            modifier = Modifier
                .height(20.dp)
        )

        summary?.let { summary ->

            HtmlText(
                html = summary,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(15.dp)
                    .wrapContentWidth(Alignment.CenterHorizontally)
            )
        }


    }

}