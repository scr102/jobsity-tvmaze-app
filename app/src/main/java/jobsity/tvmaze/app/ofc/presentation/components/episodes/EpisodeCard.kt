package jobsity.tvmaze.app.ofc.presentation.components.episodes

import android.graphics.Bitmap
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import jobsity.tvmaze.app.ofc.R
import jobsity.tvmaze.app.ofc.domain.model.episode.EpisodeItem
import jobsity.tvmaze.app.ofc.utils.IMAGE_PLACEHOLDER
import jobsity.tvmaze.app.ofc.utils.loadImage

@Composable
fun EpisodeCard (
    episode : EpisodeItem,
    onClick: () -> Unit,
){

    Card(
        shape = MaterialTheme.shapes.small,
        modifier = Modifier
            .padding(
                bottom = 6.dp,
                top = 6.dp,
            )
            .fillMaxWidth()
            .wrapContentHeight()
            .clickable(enabled = true, onClick = onClick),
        elevation = 10.dp
    ){
        Box(
            modifier = Modifier.fillMaxSize()
        ){
            Row(
                modifier = Modifier.wrapContentHeight()
            ){
                var imageBm : Bitmap? = null
                episode.image?.medium?.let { url ->
                    imageBm = loadImage(url = url, defaultImage = IMAGE_PLACEHOLDER).value
                } ?: run {
                    imageBm = loadImage(url = "", defaultImage = IMAGE_PLACEHOLDER).value
                }

                imageBm?.let { img ->
                    Image(
                        bitmap = img.asImageBitmap(),
                        stringResource(id = R.string.poster_image),
                        modifier = Modifier
                            .width(160.dp)
                            .height(90.dp),
                        contentScale = ContentScale.Crop
                    )
                }
                episode.name?.let { title->
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 12.dp, bottom = 12.dp, start = 8.dp, end = 8.dp)
                    ){
                        Text(
                            text = title,
                            modifier = Modifier
                                .fillMaxWidth(0.85f)
                                .wrapContentWidth(Alignment.Start),
                            fontSize = 18.sp,
                            color = MaterialTheme.colors.primary
                        )
                        episode.number?.let { number->
                            Text(
                                text = "Espisode ${number}",
                                modifier = Modifier
                                    .fillMaxWidth(0.85f)
                                    .wrapContentWidth(Alignment.Start) ,
                                color = MaterialTheme.colors.onSecondary
                            )
                        }
                    }
                }

            }
        }


    }
}