package jobsity.tvmaze.app.ofc.presentation.ui.episode_detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Scaffold
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import dagger.hilt.android.AndroidEntryPoint
import jobsity.tvmaze.app.ofc.presentation.components.episode_detail.EpisodeView
import jobsity.tvmaze.app.ofc.presentation.components.episodes.*
import jobsity.tvmaze.app.ofc.presentation.components.show_list.*
import jobsity.tvmaze.app.ofc.presentation.theme.AppTheme
import jobsity.tvmaze.app.ofc.presentation.ui.show_detail.ShowDetailEvent
import jobsity.tvmaze.app.ofc.presentation.ui.show_detail.ShowsDetailViewModel

@AndroidEntryPoint
class EpisodeDetailFragment : Fragment() {


    private lateinit var episodeDetailViewModel: EpisodeDetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        episodeDetailViewModel =
            ViewModelProvider(this).get(EpisodeDetailViewModel::class.java)

        getBundleArguments()

        return composeView()
    }

    private fun getBundleArguments(){
        arguments?.getInt(EPISODE_NUMBER_BUNDLE_KEY)?.let { number ->

            episodeDetailViewModel.number.value = number.toString()


        }
        arguments?.getString(EPISODE_NAME_BUNDLE_KEY)?.let { name ->

            episodeDetailViewModel.name.value = name


        }
        arguments?.getString(EPISODE_IMAGE_BUNDLE_KEY)?.let { poster ->

            episodeDetailViewModel.poster.value = poster


        }
        arguments?.getInt(EPISODE_SEASON_BUNDLE_KEY)?.let { season ->

            episodeDetailViewModel.season.value = season.toString()


        }

        arguments?.getString(EPISODE_SUMMARY_BUNDLE_KEY)?.let { summary ->

            episodeDetailViewModel.summary.value = summary


        }

    }

    private fun composeView(): ComposeView {
        return ComposeView(requireContext()).apply {
            setContent {

                AppTheme(
                    darkTheme = false
                ) {


                    val season = episodeDetailViewModel.season.value

                    val number = episodeDetailViewModel.number.value

                    val name = episodeDetailViewModel.name.value

                    val image = episodeDetailViewModel.poster.value

                    val summary = episodeDetailViewModel.summary.value


                    EpisodeView(
                        name = name,
                        number = number,
                        season = season,
                        image = image,
                        summary = summary
                    )

                }

            }

        }

    }
}
