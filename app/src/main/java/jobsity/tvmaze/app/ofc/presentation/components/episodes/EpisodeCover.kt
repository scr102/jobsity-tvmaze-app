package jobsity.tvmaze.app.ofc.presentation.components.episodes

import android.graphics.Bitmap
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import jobsity.tvmaze.app.ofc.R
import jobsity.tvmaze.app.ofc.presentation.components.HtmlText
import jobsity.tvmaze.app.ofc.utils.Genre
import jobsity.tvmaze.app.ofc.utils.IMAGE_PLACEHOLDER
import jobsity.tvmaze.app.ofc.utils.loadImage

@Composable
fun EpisodeCover(
    name : String?,
    image : String?,
    days : String?,
    genres : String?,
    time : String?,
    summary : String?
) {
    Column(){
        Row(
            modifier = Modifier.wrapContentHeight()
        ){
            var imageBm : Bitmap? = null
            image?.let { url ->
                imageBm = loadImage(url = url, defaultImage = IMAGE_PLACEHOLDER).value
            } ?: run {
                imageBm = loadImage(url = "", defaultImage = IMAGE_PLACEHOLDER).value
            }

            imageBm?.let { img ->
                Image(
                    bitmap = img.asImageBitmap(),
                    stringResource(id = R.string.poster_image),
                    modifier = Modifier
                        .width(105.dp)
                        .height(148.dp),
                    contentScale = ContentScale.Crop
                )
            }
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 12.dp, bottom = 12.dp, start = 8.dp, end = 8.dp)
            ){
                name?.let { title ->
                    Text(
                        text = title,
                        modifier = Modifier
                            .fillMaxWidth(0.85f)
                            .wrapContentWidth(Alignment.Start),
                        fontSize = 18.sp,
                        color = MaterialTheme.colors.primary
                    )
                }
                genres?.let { genres->

                    Text(
                        text = genres,
                        modifier = Modifier
                            .fillMaxWidth(0.85f)
                            .wrapContentWidth(Alignment.Start) ,
                        color = MaterialTheme.colors.onSecondary
                    )
                }
                days?.let { days->

                    Text(
                        text = days,
                        modifier = Modifier
                            .fillMaxWidth(0.85f)
                            .wrapContentWidth(Alignment.Start) ,
                        color = MaterialTheme.colors.onSecondary
                    )
                }
                time?.let { time->

                    Text(
                        text = time,
                        modifier = Modifier
                            .fillMaxWidth(0.85f)
                            .wrapContentWidth(Alignment.Start) ,
                        color = MaterialTheme.colors.onSecondary
                    )
                }
                summary?.let { summary ->
                    val openDialog = remember { mutableStateOf(false) }
                    IconButton(onClick = {
                        openDialog.value = true
                    }
                    ) {
                        Icon(Icons.Filled.Info,
                        "Summary",
                        tint= MaterialTheme.colors.primary)
                    }

                    val scroll = rememberScrollState(0)
                    if (openDialog.value) {
                        AlertDialog(
                            onDismissRequest = {
                                openDialog.value = false
                            },
                            title = {
                                Text(text = "Summary")
                            },
                            text = {
                                
                                summary?.let { summary ->
                                    HtmlText(
                                        html = summary,
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .height(200.dp)
                                            .verticalScroll(scroll)
                                            .padding(10.dp)
                                            .wrapContentWidth(Alignment.CenterHorizontally)
                                    )
                                }
                                
                            },
                            buttons = {
                                Row(
                                    modifier = Modifier.padding(all = 8.dp),
                                    horizontalArrangement = Arrangement.Center
                                ) {
                                    Button(
                                        modifier = Modifier.fillMaxWidth(),
                                        onClick = { openDialog.value = false }
                                    ) {
                                        Text("Dismiss")
                                    }
                                }
                            }
                        )
                    }
                }
            }
        }
    }

}
@Composable
fun openSummary(summary : String?){

}