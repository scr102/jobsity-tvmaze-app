package jobsity.tvmaze.app.ofc.network.model.show

import jobsity.tvmaze.app.ofc.domain.model.show.ShowItem
import jobsity.tvmaze.app.ofc.domain.util.DomainMapper
import jobsity.tvmaze.app.ofc.utils.Result


class ShowItemDtoMapper : DomainMapper<ShowItemDto, ShowItem>{
    override fun mapToDomainModel(model: ShowItemDto): ShowItem {
        return ShowItem(
                _links = model._links,
                averageRuntime = model.averageRuntime,
                externals = model.externals,
                genres = model.genres,
                id = model.id,
                image = model.image,
                language = model.language,
                name = model.name,
                network = model.network,
                officialSite = model.officialSite,
                premiered = model.premiered,
                rating = model.rating,
                runtime = model.runtime,
                schedule = model.schedule,
                status = model.status,
                summary = model.summary,
                type = model.type,
                updated = model.updated,
                url = model.url,
                webChannel = model.webChannel,
                weight = model.weight
        )
    }

    override fun mapFromDomainModel(domainModel: ShowItem): ShowItemDto {
        return ShowItemDto(
            _links = domainModel._links,
            averageRuntime = domainModel.averageRuntime,
            externals = domainModel.externals,
            genres = domainModel.genres,
            id = domainModel.id,
            image = domainModel.image,
            language = domainModel.language,
            name = domainModel.name,
            network = domainModel.network,
            officialSite = domainModel.officialSite,
            premiered = domainModel.premiered,
            rating = domainModel.rating,
            runtime = domainModel.runtime,
            schedule = domainModel.schedule,
            status = domainModel.status,
            summary = domainModel.summary,
            type = domainModel.type,
            updated = domainModel.updated,
            url = domainModel.url,
            webChannel = domainModel.webChannel,
            weight = domainModel.weight
        )
    }

    fun toDomainList(initial: List<ShowItemDto>) : List<ShowItem>{
        return initial.map { mapToDomainModel(it) }
    }

    fun fromDomainList(initial: List<ShowItem>) : List <ShowItemDto> {
        return initial.map { mapFromDomainModel(it) }
    }
}