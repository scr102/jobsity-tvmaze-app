package jobsity.tvmaze.app.ofc.network.model.episodes

import jobsity.tvmaze.app.ofc.domain.model.episode.Image
import jobsity.tvmaze.app.ofc.domain.model.episode.Links

data class EpisodeItemDto (

    val _links: Links? = null,
    val airdate: String? = null,
    val airstamp: String? = null,
    val airtime: String? = null,
    val id: Int? = null,
    val image: Image? = null,
    val name: String? = null,
    val number: Int? = null,
    val runtime: Int? = null,
    val season: Int? = null,
    val summary: String? = null,
    val type: String? = null,
    val url: String? = null

)