package jobsity.tvmaze.app.ofc.presentation.components.show_list

import android.os.Bundle
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import jobsity.tvmaze.app.ofc.R
import jobsity.tvmaze.app.ofc.domain.model.show.ShowItem
import jobsity.tvmaze.app.ofc.presentation.components.CircularProgressBar
import jobsity.tvmaze.app.ofc.presentation.components.anim.LoadingShowListShimmer
import jobsity.tvmaze.app.ofc.presentation.ui.shows_list.ShowsListEvent
import jobsity.tvmaze.app.ofc.utils.Days
import jobsity.tvmaze.app.ofc.utils.Genre

const val SHOW_ID_BUNDLE_KEY = "showId"
const val SHOW_NAME_BUNDLE_KEY = "showName"
const val SHOW_DAYS_BUNDLE_KEY = "showDays"
const val SHOW_TIME_BUNDLE_KEY = "showTime"
const val SHOW_GENRES_BUNDLE_KEY = "showGenres"
const val SHOW_SUMMARY_BUNDLE_KEY = "showSummary"
const val SHOW_POSTER_BUNDLE_KEY = "showPoster"


@Composable
fun ShowList (
    loading : Boolean,
    shows : List<ShowItem>,
    onChangeScrollPosition : (Int) -> Unit,
    onTriggerEvent : (ShowsListEvent) -> Unit,
    pageSize : Int,
    searching : Boolean,
    navController: NavController
){
    Box(
        modifier = Modifier.fillMaxSize()
            .background(color = MaterialTheme.colors.surface)
    ) {
        if (loading && shows.isEmpty()) {
            LoadingShowListShimmer(
                coverHeight = 148.dp,
                coverWidth = 105.dp,
                textHeight = 18.dp,
                textWidth = 100.dp
            )
        }
        LazyColumn{
            itemsIndexed(
                items = shows
            ) { index, show ->
                onChangeScrollPosition(index)
                if((index + 1 >= pageSize) && !loading && !searching){
                    onTriggerEvent(ShowsListEvent.NextPageEvent)
                }
                ShowsCard(show = show, onClick = {
                    val bundle = Bundle()
                    show.id?.let {
                        bundle.putInt(SHOW_ID_BUNDLE_KEY, it)
                    }
                    bundle.putString(SHOW_NAME_BUNDLE_KEY, show.name)
                    bundle.putString(SHOW_POSTER_BUNDLE_KEY, show.image?.medium)
                    bundle.putString(SHOW_DAYS_BUNDLE_KEY, Days.getDays(show.schedule?.days))
                    bundle.putString(SHOW_TIME_BUNDLE_KEY, show.schedule?.time)
                    bundle.putString(SHOW_GENRES_BUNDLE_KEY, Genre.getGenre(show.genres))
                    bundle.putString(SHOW_SUMMARY_BUNDLE_KEY, show.summary)

                    navController.navigate(R.id.viewShow, bundle)
                })

            }

        }
        CircularProgressBar(isDisplay = loading)
    }
}