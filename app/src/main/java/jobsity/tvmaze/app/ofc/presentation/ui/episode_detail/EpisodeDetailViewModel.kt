package jobsity.tvmaze.app.ofc.presentation.ui.episode_detail

import androidx.compose.runtime.mutableStateOf
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import jobsity.tvmaze.app.ofc.domain.model.episode.EpisodeItem
import jobsity.tvmaze.app.ofc.presentation.ui.show_detail.ShowDetailEvent
import kotlinx.coroutines.launch

class EpisodeDetailViewModel @ViewModelInject constructor(
) : ViewModel() {

    val showId = mutableStateOf(1)

    val name = mutableStateOf("")

    val number = mutableStateOf("")

    val poster = mutableStateOf("")

    val summary = mutableStateOf("")

    val season = mutableStateOf("1")


}