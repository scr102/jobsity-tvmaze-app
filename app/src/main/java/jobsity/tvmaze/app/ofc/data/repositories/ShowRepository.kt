package jobsity.tvmaze.app.ofc.data.repositories

import dagger.Module
import dagger.Provides
import jobsity.tvmaze.app.ofc.utils.Result

import jobsity.tvmaze.app.ofc.domain.model.show.ShowItem
import kotlinx.coroutines.flow.Flow

interface ShowRepository {
    suspend fun getShows(page: Int):List<ShowItem>

    suspend fun searchShows(query: String): List<ShowItem>

}