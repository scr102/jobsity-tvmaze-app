package jobsity.tvmaze.app.ofc.utils

class Genre {

    companion object{
        fun getGenre(ids: List<String>?) : String? {
            ids?.let {
                val genreStrs = mutableListOf<String>()
                ids.forEach {
                    genreStrs.add(it)
                }
                return genreStrs.joinToString(separator = ", ")
            } ?: return  ""
        }
    }

}