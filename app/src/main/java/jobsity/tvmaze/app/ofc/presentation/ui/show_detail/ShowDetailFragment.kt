package jobsity.tvmaze.app.ofc.presentation.ui.show_detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Scaffold
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import dagger.hilt.android.AndroidEntryPoint
import jobsity.tvmaze.app.ofc.presentation.components.episodes.EpisodeCover
import jobsity.tvmaze.app.ofc.presentation.components.episodes.EpisodesList
import jobsity.tvmaze.app.ofc.presentation.components.episodes.SeasonList
import jobsity.tvmaze.app.ofc.presentation.components.episodes.SeasonTab
import jobsity.tvmaze.app.ofc.presentation.components.show_list.*
import jobsity.tvmaze.app.ofc.presentation.theme.AppTheme
import jobsity.tvmaze.app.ofc.utils.Days
import jobsity.tvmaze.app.ofc.utils.Genre

@AndroidEntryPoint
class ShowDetailFragment : Fragment() {


    private lateinit var showDetailViewModel: ShowsDetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        showDetailViewModel =
            ViewModelProvider(this).get(ShowsDetailViewModel::class.java)

        getBundleArguments()

        showDetailViewModel.onTriggerEvent(ShowDetailEvent.FetchEpisodeEvent)

        return composeView()
    }

    private fun getBundleArguments(){
        arguments?.getInt(SHOW_ID_BUNDLE_KEY)?.let { sId ->

            showDetailViewModel.showId.value = sId


        }
        arguments?.getString(SHOW_NAME_BUNDLE_KEY)?.let { name ->

            showDetailViewModel.name.value = name


        }
        arguments?.getString(SHOW_POSTER_BUNDLE_KEY)?.let { poster ->

            showDetailViewModel.poster.value = poster


        }
        arguments?.getString(SHOW_DAYS_BUNDLE_KEY)?.let { days ->

            showDetailViewModel.days.value = days


        }
        arguments?.getString(SHOW_TIME_BUNDLE_KEY)?.let { time ->

            showDetailViewModel.time.value = time


        }
        arguments?.getString(SHOW_GENRES_BUNDLE_KEY)?.let { genres ->

            showDetailViewModel.genres.value = genres


        }
        arguments?.getString(SHOW_SUMMARY_BUNDLE_KEY)?.let { summary ->

            showDetailViewModel.summary.value = summary


        }

    }

    private fun composeView(): ComposeView {
        return ComposeView(requireContext()).apply {
            setContent {

                AppTheme(
                    darkTheme = false
                ) {

                    val loading = showDetailViewModel.loading.value

                    val episodes = showDetailViewModel.episodeSeasonlist.value

                    val season = showDetailViewModel.season.value

                    val seasonList = showDetailViewModel.seasonList.value

                    val name = showDetailViewModel.name.value

                    val image = showDetailViewModel.poster.value

                    val days = showDetailViewModel.days.value

                    val time = showDetailViewModel.time.value

                    val genres = showDetailViewModel.genres.value

                    val summary = showDetailViewModel.summary.value


                    Scaffold(
                        topBar = {
                            EpisodeCover(
                                name = name,
                                image = image,
                                days = days,
                                genres = genres,
                                time = time,
                                summary = summary
                            )
                        }
                    ) {
                        Column(){
                            SeasonList(
                                seasons= seasonList,
                                onSelectedSeasonChanged = showDetailViewModel::onSelectedSeasonChanged,
                                onGetSeason = showDetailViewModel::getEpisodesBySeason,
                                selectedSeason = season
                            )
                            EpisodesList(
                                loading = loading,
                                episodes = episodes,
                                onChangeScrollPosition = showDetailViewModel::onChangeScrollPosition,
                                onTriggerEvent = showDetailViewModel::onTriggerEvent,
                                navController = findNavController()
                            )
                        }


                    }

                }

            }

        }

    }
}
