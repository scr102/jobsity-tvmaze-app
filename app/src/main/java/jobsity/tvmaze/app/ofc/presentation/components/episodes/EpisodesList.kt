package jobsity.tvmaze.app.ofc.presentation.components.episodes

import android.os.Bundle
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import jobsity.tvmaze.app.ofc.R
import jobsity.tvmaze.app.ofc.domain.model.episode.EpisodeItem
import jobsity.tvmaze.app.ofc.presentation.components.CircularProgressBar
import jobsity.tvmaze.app.ofc.presentation.components.anim.LoadingShowListShimmer
import jobsity.tvmaze.app.ofc.presentation.ui.show_detail.ShowDetailEvent

const val EPISODE_NAME_BUNDLE_KEY = "episodeName"
const val EPISODE_NUMBER_BUNDLE_KEY = "episodeNumber"
const val EPISODE_SEASON_BUNDLE_KEY = "episodeSeason"
const val EPISODE_SUMMARY_BUNDLE_KEY = "episodeSummary"
const val EPISODE_IMAGE_BUNDLE_KEY = "episodeImage"


@Composable
fun EpisodesList (
    loading : Boolean,
    episodes : List<EpisodeItem>,
    onChangeScrollPosition : (Int) -> Unit,
    onTriggerEvent : (ShowDetailEvent) -> Unit,
    navController: NavController
){
    Box(
        modifier = Modifier.fillMaxWidth()
            .background(color = MaterialTheme.colors.surface)
    ) {
        if (loading && episodes.isEmpty()) {
            LoadingShowListShimmer(
                coverHeight = 90.dp,
                coverWidth = 160.dp,
                textHeight = 18.dp,
                textWidth = 100.dp
            )
        }
        LazyColumn{
            itemsIndexed(
                items = episodes
            ) { index, episode ->
                onChangeScrollPosition(index)
                EpisodeCard(episode = episode, onClick = {
                    val bundle = Bundle()
                    bundle.putString(EPISODE_NAME_BUNDLE_KEY, episode.name)
                    episode.number?.let { bundle.putInt(EPISODE_NUMBER_BUNDLE_KEY, it) }
                    episode.season?.let { bundle.putInt(EPISODE_SEASON_BUNDLE_KEY, it) }
                    bundle.putString(EPISODE_SUMMARY_BUNDLE_KEY, episode.summary)
                    bundle.putString(EPISODE_IMAGE_BUNDLE_KEY, episode.image?.medium)
                    navController.navigate(R.id.viewEpisode, bundle)
                })

            }

        }
        CircularProgressBar(isDisplay = loading)
    }
}