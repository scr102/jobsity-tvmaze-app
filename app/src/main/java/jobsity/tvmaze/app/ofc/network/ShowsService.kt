package jobsity.tvmaze.app.ofc.network

import jobsity.tvmaze.app.ofc.network.model.episodes.EpisodeItemDto
import jobsity.tvmaze.app.ofc.network.model.show.SearchShowItemDto
import jobsity.tvmaze.app.ofc.network.model.show.ShowItemDto
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ShowsService {

    @GET("shows")
    suspend fun getShows(@Query("page") page : Int) : List<ShowItemDto>

    @GET("search/shows")
    suspend fun searchShows(@Query("q") search : String) : List<SearchShowItemDto>

    @GET("shows/{showId}/episodes")
    suspend fun getEpisodes(@Path("showId") showId : Int) : List<EpisodeItemDto>

}