package jobsity.tvmaze.app.ofc.presentation.components.show_list

import android.graphics.Bitmap
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import jobsity.tvmaze.app.ofc.R
import jobsity.tvmaze.app.ofc.domain.model.show.ShowItem
import jobsity.tvmaze.app.ofc.utils.Genre
import jobsity.tvmaze.app.ofc.utils.IMAGE_PLACEHOLDER
import jobsity.tvmaze.app.ofc.utils.loadImage

@Composable
fun ShowsCard (
    show : ShowItem,
    onClick: () -> Unit,
){

    Card(
        shape = MaterialTheme.shapes.small,
        modifier = Modifier
            .padding(
                bottom = 6.dp,
                top = 6.dp,
            )
            .fillMaxWidth()
            .wrapContentHeight()
            .clickable(enabled = true, onClick = onClick),
        elevation = 10.dp
    ){
        Box(
            modifier = Modifier.fillMaxSize()
        ){
            Row(
                modifier = Modifier.wrapContentHeight()
            ){
                var imageBm : Bitmap? = null
                show.image?.medium?.let { url ->
                    imageBm = loadImage(url = url, defaultImage = IMAGE_PLACEHOLDER).value
                } ?: run {
                    imageBm = loadImage(url = "", defaultImage = IMAGE_PLACEHOLDER).value
                }

                imageBm?.let { img ->
                    Image(
                        bitmap = img.asImageBitmap(),
                        stringResource(id = R.string.poster_image),
                        modifier = Modifier
                            .width(105.dp)
                            .height(148.dp),
                        contentScale = ContentScale.Crop
                    )
                }
                show.name?.let { title->
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 12.dp, bottom = 12.dp, start = 8.dp, end = 8.dp)
                    ){
                        Text(
                            text = title,
                            modifier = Modifier
                                .fillMaxWidth(0.85f)
                                .wrapContentWidth(Alignment.Start),
                            fontSize = 18.sp,
                            color = MaterialTheme.colors.primary
                        )
                        show.genres?.let { genres->
                            val genre = Genre.getGenre(genres)

                            Text(
                                text = genre!!,
                                modifier = Modifier
                                    .fillMaxWidth(0.85f)
                                    .wrapContentWidth(Alignment.Start) ,
                                color = MaterialTheme.colors.onSecondary
                            )
                        }
                    }
                }

            }
            IconButton(
                onClick = {},
                modifier = Modifier
                    .width(45.dp)
                    .height(45.dp)
                    .align(Alignment.BottomEnd)
                    .padding(10.dp)
            ){
                Icon(
                    Icons.Filled.FavoriteBorder,
                    "Add to favorite",
                    tint = MaterialTheme.colors.primary

                )
            }
        }


    }
}