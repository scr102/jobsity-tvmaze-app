package jobsity.tvmaze.app.ofc.domain.model.episode

data class Image(
    val medium: String,
    val original: String
)