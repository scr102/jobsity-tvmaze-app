package jobsity.tvmaze.app.ofc.network.model.show

import jobsity.tvmaze.app.ofc.domain.model.show.SearchShowItem
import jobsity.tvmaze.app.ofc.domain.model.show.ShowItem
import jobsity.tvmaze.app.ofc.domain.util.DomainMapper

class SearchShowItemDtoMapper : DomainMapper<SearchShowItemDto, ShowItem> {
    override fun mapToDomainModel(model: SearchShowItemDto): ShowItem {
        return ShowItem(
            _links = model.show?._links,
            averageRuntime = model.show?.averageRuntime,
            externals = model.show?.externals,
            genres = model.show?.genres,
            id = model.show?.id,
            image = model.show?.image,
            language = model.show?.language,
            name = model.show?.name,
            network = model.show?.network,
            officialSite = model.show?.officialSite,
            premiered = model.show?.premiered,
            rating = model.show?.rating,
            runtime = model.show?.runtime,
            schedule = model.show?.schedule,
            status = model.show?.status,
            summary = model.show?.summary,
            type = model.show?.type,
            updated = model.show?.updated,
            url = model.show?.url,
            webChannel = model.show?.webChannel,
            weight = model.show?.weight
        )
    }

    override fun mapFromDomainModel(domainModel: ShowItem): SearchShowItemDto {
        return SearchShowItemDto(
            score = 0f,
            show = domainModel
        )
    }

    fun toDomainList(initial: List<SearchShowItemDto>): List<ShowItem> {
        return initial.map { mapToDomainModel(it) }
    }

    fun fromDomainList(initial: List<ShowItem>): List<SearchShowItemDto> {
        return initial.map { mapFromDomainModel(it) }
    }

}